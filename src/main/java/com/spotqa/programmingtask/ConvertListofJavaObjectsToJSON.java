package com.spotqa.programmingtask;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ConvertListofJavaObjectsToJSON {
	
	public JSONObject convert(List<DataObject> employees) {
		JSONObject jsonObj = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		for (DataObject emp : employees) {
			JSONObject jsonEmpObj = new JSONObject();
			jsonEmpObj.put("id", emp.getId());
			jsonEmpObj.put("firstName", emp.getFirstName());
			jsonEmpObj.put("lastName", emp.getLastName());
			jsonEmpObj.put("birthdate", emp.getBirthdate());
			
			JSONObject employeeid=new JSONObject();
			employeeid.put(emp.getId()+"", jsonEmpObj);
			
			JSONObject employeestag=new JSONObject();
			employeestag.put("employees", employeeid);
			JSONObject managerid=new JSONObject();
			managerid.put(emp.getManagers(), employeestag);
			JSONObject jsonManager=new JSONObject();
			jsonManager.put("managers", managerid);
			
			
			JSONObject teamid=new JSONObject();
			teamid.put(emp.getTeams(), jsonManager);
			JSONObject teams=new JSONObject();
			teams.put("teams", teamid);
			
			JSONObject divisionid=new JSONObject();
			divisionid.put(emp.getDivisions(), teams);
			JSONObject divisions=new JSONObject();
			divisions.put("divisions", divisionid);
			
			jsonArray.put(divisions);
		}
		
		jsonObj.put("", jsonArray);
		
		System.out.println(jsonObj);
		
		return jsonObj;
	}

}
