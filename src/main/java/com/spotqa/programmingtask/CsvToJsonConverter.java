package com.spotqa.programmingtask;

import java.io.*;
import java.util.*;

public class CsvToJsonConverter {

	public static void main(String args[]) throws IOException {
		File csvFile = new File("data.csv");
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csvFile));
		String line = bufferedReader.readLine();
		List<DataObject> employees = new ArrayList<>();
		while (line != null) {
			String[] eachelement = line.split(",");
			if (eachelement[3].matches(".*\\d.*")) {
				int id = Integer.valueOf(eachelement[3]);
				DataObject employee = new DataObject(eachelement[0], eachelement[1], eachelement[2], id, eachelement[4],
						eachelement[5], eachelement[6]);
				employees.add(employee);
			}
			line = bufferedReader.readLine();
		}
		ConvertListofJavaObjectsToJSON converttoJson = new ConvertListofJavaObjectsToJSON();
		converttoJson.convert(employees);
		
		CsvToJsonConverterSort sortById=new CsvToJsonConverterSort();
		sortById.sortByIdAndConvert(employees);
		
		CsvToJsonConverterSort sortByFName=new CsvToJsonConverterSort();
		sortByFName.sortByFNameAndConvert(employees);
		
		CsvToJsonConverterSort sortByLName=new CsvToJsonConverterSort();
		sortByLName.sortByLNameAndConvert(employees);
		
		bufferedReader.close();

	}

}
