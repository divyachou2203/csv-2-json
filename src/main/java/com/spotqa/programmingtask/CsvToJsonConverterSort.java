package com.spotqa.programmingtask;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.JSONObject;

public class CsvToJsonConverterSort {

	public JSONObject sortByIdAndConvert(List<DataObject> employees){
		//Sort By EmployeeId
		Comparator<DataObject> compareById = (DataObject do1, DataObject do2) -> do1.getId().compareTo(do2.getId());
		Collections.sort(employees,compareById);
		
		ConvertListofJavaObjectsToJSON converttoJson=new ConvertListofJavaObjectsToJSON();
		return converttoJson.convert(employees);
	}
	
	public JSONObject sortByFNameAndConvert(List<DataObject> employees){
		//Sort By FName
		Comparator<DataObject> compareByFName = (DataObject do1, DataObject do2) -> do1.getFirstName().compareTo(do2.getFirstName());
			Collections.sort(employees,compareByFName);
		
		ConvertListofJavaObjectsToJSON converttoJson=new ConvertListofJavaObjectsToJSON();
		return converttoJson.convert(employees);
	}
	
	public JSONObject sortByLNameAndConvert(List<DataObject> employees){
		//Sort By LName
		Comparator<DataObject> compareByLName = (DataObject do1, DataObject do2) -> do1.getLastName().compareTo(do2.getLastName());
		Collections.sort(employees,compareByLName);
		
		ConvertListofJavaObjectsToJSON converttoJson=new ConvertListofJavaObjectsToJSON();
		return converttoJson.convert(employees);
	}
}
