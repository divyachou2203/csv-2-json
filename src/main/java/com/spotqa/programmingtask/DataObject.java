package com.spotqa.programmingtask;

public class DataObject {
	
	private String divisions;
	private String teams;
	private String managers;
	private Integer id;
	private String firstName;
	private String lastName;
	private String birthdate;
	
	public DataObject(String divisions,String teams,String managers,Integer id,String firstName,String lastName,String birthdate) {
		this.divisions=divisions;
		this.teams=teams;
		this.managers=managers;
		this.id=id;
		this.firstName=firstName;
		this.lastName=lastName;
		this.birthdate=birthdate;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getDivisions() {
		return divisions;
	}

	public void setDivisions(String divisions) {
		this.divisions = divisions;
	}

	public String getTeams() {
		return teams;
	}

	public void setTeams(String teams) {
		this.teams = teams;
	}

	public String getManagers() {
		return managers;
	}

	public void setManagers(String managers) {
		this.managers = managers;
	}
	
	

}
