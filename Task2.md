Solution:

1.       Web Domain/URL finder Process

Which generates(finds) all available pages from given domain and subdomains

2.       Web Crawler Process

This is multithreaded asynchronous process, which opens one url per thread from its respective url pool(In memory cache and we need to handle concurrency there).

Each Thread opens a new uri from threads parent Crawler process pool, in asynchronous manner completion of webpage loading.

This way each thread is not blocked while the server responds to page request.

We can figure out optimal thread count for this process based on server on which its hosted.

3.       Master Seed Process

Its job is to spin-up configurations for 1 and 2 processes.

Except Master seed process, other two process are spanned across multiple machines like 20 to 30 or if cloud environment is available, we may take advantage of it and spawn as many machines are required to process all urls quickly.
